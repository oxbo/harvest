from arena_api.system import system
from arena_api.buffer import *
import ctypes
import numpy as np
import cv2
import time
import socket
import time
import json
import os

folder_to_save = f"/mnt/data"
if not os.path.exists(folder_to_save): os.makedirs(folder_to_save)


class ManimaApiCommunication:
    def __init__(self, udp_ip, udp_port):
        self.udp_ip = udp_ip
        self.udp_port = udp_port

    def get_parameter_value(self, parameter_name):
        if not parameter_name: raise ValueError("Parameter name cannot be empty.")
        message = json.dumps({"jsonrpc": "2.0", "method": "getParameter", "params": {"Name": parameter_name}, "id": 15})
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
            try:
                sock.sendto(message.encode("utf-8"), (self.udp_ip, self.udp_port))
                data, server_address = sock.recvfrom(4096)
                reply_string = data.decode("utf-8")
                return reply_string
            except socket.error as e:
                print(f"Socket error: {e}")
                return None

    def set_parameter_value(self, parameter_name, value):
        if not parameter_name: raise ValueError("Parameter name cannot be empty.")
        message = json.dumps({"jsonrpc": "2.0", "method": "setParameter", "params": {"Name": parameter_name, "Value": value}, "id": 15})
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
            try:
                sock.sendto(message.encode("utf-8"), (self.udp_ip, self.udp_port))
                data, server_address = sock.recvfrom(4096)
                reply_string = data.decode("utf-8")
                return reply_string
            except socket.error as e:
                print(f"Socket error: {e}")
                return None

    def set_scene_duty(self, port_nr, value):
        if not isinstance(port_nr, int) or port_nr < 1 or port_nr > 8: raise ValueError("Port number must be an integer between 1 and 8.")
        if not isinstance(value, int) or value < 0 or value > 255: raise ValueError("Value must be an integer between 0 and 255.")
        parameter_name = f"Port {port_nr} Scene Duty"
        return self.set_parameter_value(parameter_name, value)


def create_devices_with_tries():
    tries = 0
    tries_max = 6
    sleep_time_secs = 10
    while tries < tries_max:  # Wait for device for 60 seconds
        devices = system.create_device()
        if not devices:
            print(
                f'Try {tries+1} of {tries_max}: waiting for {sleep_time_secs} '
                f'secs for a device to be connected!')
            for sec_count in range(sleep_time_secs):
                time.sleep(1)
                print(f'{sec_count + 1 } seconds passed ',
                    '.' * sec_count, end='\r')
            tries += 1
        else:
            print(f'Created {len(devices)} device(s)')
            return devices
    else:
        raise Exception(f'No device found! Please connect a device and run '
                        f'the example again.')


def setup(device):
    nodemap = device.nodemap
    nodes = nodemap.get_node(['Width', 'Height', 'PixelFormat'])
    nodes['Width'].value = nodes['Width'].max
    nodes['Height'].value = nodes['Height'].max
    nodes['PixelFormat'].value = 'BGR8'
    num_channels = 3
    tl_stream_nodemap = device.tl_stream_nodemap
    tl_stream_nodemap["StreamBufferHandlingMode"].value = "NewestOnly"
    tl_stream_nodemap['StreamAutoNegotiatePacketSize'].value = True
    tl_stream_nodemap['StreamPacketResendEnable'].value = True
    return num_channels


def example_entry_point(controller: ManimaApiCommunication):
    device = None
    devices = create_devices_with_tries()
    for dev in devices:
        name = dev.nodemap['GevMACAddress'].value
        if name == 30853686650532:
            device = dev
    if device is None:
        raise Exception('Could not find a device with MAC 30853686650532')

    num_channels = setup(device)
    time_to_heatup = 0.25
    with device.start_stream():
        loop_count = 0
        while True:
            unix_stamp = time.time()
            loop_count += 1
            print(controller.set_scene_duty(1, 250))
            time.sleep(time_to_heatup)
            buffer = device.get_buffer()
            item = BufferFactory.copy(buffer)
            device.requeue_buffer(buffer)
            buffer_bytes_per_pixel = int(len(item.data)/(item.width * item.height))
            array = (ctypes.c_ubyte * num_channels * item.width * item.height).from_address(ctypes.addressof(item.pbytes))
            npndarray = np.ndarray(buffer=array, dtype=np.uint8, shape=(item.height, item.width, buffer_bytes_per_pixel))
            # cv2.imshow('Lucid', npndarray)
            #same image
            cv2.imwrite(f'images/image_{loop_count}.jpg', npndarray)
            BufferFactory.destroy(item)
            print(controller.set_scene_duty(1, 0))
            time.sleep(0.5)
            key = cv2.waitKey(1)
            if key == 27:
                break
            
        device.stop_stream()
        cv2.destroyAllWindows()
    
    system.destroy_device()


if __name__ == '__main__':
    print('\nWARNING:\nTHIS EXAMPLE MIGHT CHANGE THE DEVICE(S) SETTINGS!')
    print('\nExample started\n')
    controller = ManimaApiCommunication("192.168.0.118", 6512)
    example_entry_point(controller)
    print('\nExample finished successfully')
