import time
from arena_api.enums import PixelFormat
from arena_api.__future__.save import Writer
from arena_api.system import system
from arena_api.buffer import BufferFactory
import cv2 
import numpy as np
import socket
import time
import json
import os

folder_to_save = f"/mnt/data"
if not os.path.exists(folder_to_save): os.makedirs(folder_to_save)

class ManimaApiCommunication:
    def __init__(self, udp_ip, udp_port):
        self.udp_ip = udp_ip
        self.udp_port = udp_port

    def get_parameter_value(self, parameter_name):
        if not parameter_name: raise ValueError("Parameter name cannot be empty.")
        message = json.dumps({"jsonrpc": "2.0", "method": "getParameter", "params": {"Name": parameter_name}, "id": 15})
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
            try:
                sock.sendto(message.encode("utf-8"), (self.udp_ip, self.udp_port))
                data, server_address = sock.recvfrom(4096)
                reply_string = data.decode("utf-8")
                return reply_string
            except socket.error as e:
                print(f"Socket error: {e}")
                return None

    def set_parameter_value(self, parameter_name, value):
        if not parameter_name: raise ValueError("Parameter name cannot be empty.")
        message = json.dumps({"jsonrpc": "2.0", "method": "setParameter", "params": {"Name": parameter_name, "Value": value}, "id": 15})
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
            try:
                sock.sendto(message.encode("utf-8"), (self.udp_ip, self.udp_port))
                data, server_address = sock.recvfrom(4096)
                reply_string = data.decode("utf-8")
                return reply_string
            except socket.error as e:
                print(f"Socket error: {e}")
                return None

    def set_scene_duty(self, port_nr, value):
        if not isinstance(port_nr, int) or port_nr < 1 or port_nr > 8: raise ValueError("Port number must be an integer between 1 and 8.")
        if not isinstance(value, int) or value < 0 or value > 255: raise ValueError("Value must be an integer between 0 and 255.")
        parameter_name = f"Port {port_nr} Scene Duty"
        return self.set_parameter_value(parameter_name, value)


def create_device_with_tries():
    tries = 0
    tries_max = 6
    sleep_time_secs = 10
    devices = None
    while tries < tries_max:
        devices = system.create_device()
        if not devices:
            print(
                f'Try {tries+1} of {tries_max}: waiting for {sleep_time_secs} '
                f'secs for a device to be connected!')
            for sec_count in range(sleep_time_secs):
                time.sleep(1)
                print(f'{sec_count + 1 } seconds passed ',
                    '.' * sec_count, end='\r')
            tries += 1
        else:
            return devices
    else:
        raise Exception(f'No device found! Please connect a device and run the example again.')

def save(buffer, name: str):
    converted = BufferFactory.convert(buffer, PixelFormat.BGR8)
    print(f"Converted image to {PixelFormat.BGR8.name}")
    print(f'Prepare Image Writer')
    writer = Writer()
    writer.pattern = f'images/image_{name}.jpg'
    writer.save(converted)
    print(f'Image saved')
    BufferFactory.destroy(converted)


def main_loop(controller: ManimaApiCommunication):
    devices = create_device_with_tries()
    looper = 0
    while True:
        looper += 1
        for device in devices:
            name = device.nodemap['GevMACAddress'].value
            if name == 30853686650532:
                print(controller.set_scene_duty(1, 250))
            tl_stream_nodemap = device.tl_stream_nodemap
            tl_stream_nodemap['StreamAutoNegotiatePacketSize'].value = True
            tl_stream_nodemap['StreamPacketResendEnable'].value = True
            device.start_stream()
            buffer = device.get_buffer()
            save(buffer, str(name) + str(looper))
            device.requeue_buffer(buffer)
            device.stop_stream()
            # system.destroy_device(device)
            print(controller.set_scene_duty(1, 0))
            time.sleep(5)


if __name__ == "__main__":
    print("Example Started\n")
    controller = ManimaApiCommunication("192.168.0.118", 6512)
    main_loop(controller)
    print("\nExample Completed")
