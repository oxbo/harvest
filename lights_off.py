import socket
import time
import json

class ManimaApiCommunication:
    def __init__(self, udp_ip, udp_port):
        self.udp_ip = udp_ip
        self.udp_port = udp_port

    def get_parameter_value(self, parameter_name):
        """
        Get the value of a parameter from the Manima API.
        Args:
            parameter_name (str): The name of the parameter.
        Returns:
            str: The value of the parameter, or None if an error occurred.
        """
        if not parameter_name:
            raise ValueError("Parameter name cannot be empty.")

        message = json.dumps({
            "jsonrpc": "2.0",
            "method": "getParameter",
            "params": {"Name": parameter_name},
            "id": 15
        })

        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
            try:
                sock.sendto(message.encode("utf-8"), (self.udp_ip, self.udp_port))
                data, server_address = sock.recvfrom(4096)
                reply_string = data.decode("utf-8")
                return reply_string
            except socket.error as e:
                print(f"Socket error: {e}")
                return None

    def set_parameter_value(self, parameter_name, value):
        """
        Set the value of a parameter in the Manima API.
        Args:
            parameter_name (str): The name of the parameter.
            value (int): The value to set.
        Returns:
            str: The reply from the Manima API, or None if an error occurred.
        """
        if not parameter_name:
            raise ValueError("Parameter name cannot be empty.")

        message = json.dumps({
            "jsonrpc": "2.0",
            "method": "setParameter",
            "params": {"Name": parameter_name, "Value": value},
            "id": 15
        })

        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
            try:
                sock.sendto(message.encode("utf-8"), (self.udp_ip, self.udp_port))
                data, server_address = sock.recvfrom(4096)
                reply_string = data.decode("utf-8")
                return reply_string
            except socket.error as e:
                print(f"Socket error: {e}")
                return None

    def set_scene_duty(self, port_nr, value):
        """
        Set the duty cycle value of a scene for a specific port.
        Args:
            port_nr (int): The port number (1-8).
            value (int): The duty cycle value (0-255).
        Returns:
            str: The reply from the Manima API, or None if an error occurred.
        """
        if not isinstance(port_nr, int) or port_nr < 1 or port_nr > 8:
            raise ValueError("Port number must be an integer between 1 and 8.")
        
        if not isinstance(value, int) or value < 0 or value > 255:
            raise ValueError("Value must be an integer between 0 and 255.")

        parameter_name = f"Port {port_nr} Scene Duty"
        return self.set_parameter_value(parameter_name, value)


import signal
import sys

def signal_handler(sig, frame):
    test.set_scene_duty(1, 0)
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

if __name__ == "__main__":
    sleep = 0.5
    devices = ["192.168.1.140", "192.168.1.125", "192.168.1.126"]
    for device in devices:
        test = ManimaApiCommunication(device, 6512)
        print(test.get_parameter_value("NTC1Temp"))
        for i in range(8):
            print(test.set_scene_duty(i+1, 0))
            time.sleep(sleep)
