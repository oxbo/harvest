import time
import os

os.environ["ArenaSDK"] = "/doc/work/WUR/data/OXBO/code/cameras/arena"

from arena_api.system import system
explore_access = True
explore_visibility = True
explore_type = True
explore_value = True

def create_devices_with_tries():
    tries = 0
    tries_max = 6
    sleep_time_secs = 10
    devices = None
    while tries < tries_max:
        devices = system.create_device()
        if not devices:
            print(
                f'Try {tries+1} of {tries_max}: waiting for {sleep_time_secs} '
                f'secs for a device to be connected!')
            for sec_count in range(sleep_time_secs):
                time.sleep(1)
                print(f'{sec_count + 1 } seconds passed ',
                    '.' * sec_count, end='\r')
            tries += 1
        else:
            print(f'Created {len(devices)} de vice(s)')
            return devices
    else:
        raise Exception(f'No device found! Please connect a device and run '
                        f'the example again.')
    
def main():
    device = None
    devices = create_devices_with_tries()
    device = devices[0]
    # for dev in devices:
    #     name = dev.nodemap['GevMACAddress'].value
    #     if name == 30853686650532:
    #         device = dev
    # if device is None:
    #     raise Exception('Could not find a device with MAC 30853686650532')

    nodemap = device.nodemap
    nodemap["AutoExposureAOIEnable"].value = True
    nodemap["AutoExposureAOIOffsetX"].value = 1300
    nodemap["AutoExposureAOIOffsetY"].value = 600
    nodemap["AutoExposureAOIHeight"].value = 30
    nodemap["AutoExposureAOIWidth"].value = 30
    # for node_name in nodemap.feature_names: print(nodemap[node_name])
    print(device.nodemap.get_node("GevMACAddress").value)
    system.destroy_device(device)

if __name__ == "__main__":
    main()
